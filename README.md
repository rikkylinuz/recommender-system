<h2> Recommender system</h2>

1.	Recommended movies based on the user profile i.e movies similar to which user has liked/highly rated. <br>
This is done using k-Nearest Neighbors and kmeans clustering.
2.	Movies can also be recommended by predicting the rating the users would give to certain movie and recommend based on that. <br>
    This is done using two techniques:
    1. Multi Layer perceptron
    2. Neural Matrix Factorization(is better compared using Root mean square error)

<h4>Type 1 Recommendation:</h4>

	Input:  Around 26 user profiles is created with details userid, movieid, ratings. 
            userId 1-25 are normal users who has watched and rated few movies. userId = 100 is new user. 
    Identify movies which user likes from input: Written logic to find top few movies which the user has liked and rated the most. 
            Will use those identified movies as input to our model KNN and Kmeans. 
            If it is new user then highly rated movies from popular genre will be recommended.
	K-Nearest Neighbor: 
		This model uses Euclidean distance as measure, and algorithm uses brute. 
		Because the above performs better when compared with measures and algorithms like cosine, kd-trees.
		Fuzzy matching is used to find the exact movie name and id even if it is found or typed wrongly. And it also used in kMeans to identify the movie name.
		Model is fitted with the training data and nearest neighbors movies are found with closest distance to the test data. 
		And this identified 10 closest movies are recommended.
	K-Means Clustering:
		Kmeans++ is used to initialize cluster.
		Elbow method is used to identify the right number of clusters required to cluster movies perfectly. 

<h4>Type 2 Recommendation:</h4>

    Train/Test data:    80% Train data, 20% Test data
	Input:  Users profiles i.e userId,MovieId,ratings will be given as the input data.
	Predict:    How user will rate a particular movie?
	
	Models:
	Common: Data dimensionality is reduced at the beginning.
        1.Multi Layer Perceptron:
            - Model is created using keras tensorflow frameworks. Used distinguished layers, trained the model based on training data EPOCH =15.
            - Will predict the test data rating by loading the trained MLP model, and Used Root mean square error measure to evaluate the model.

        2.Neural Matrix Model:
            - The model is trained with the training data for EPOCH configured as 15, also callback early stoping is used.
            - Then will predict the test data rating by loading the trained Neural Matrix Model, and used Root mean square error measure to evaluate  the model.

Evaluating models:<br>
Root square mean error outputs:<br>
MLP - The out-of-sample RMSE of rating predictions using MLP is 1.2184253<br>
Neural Matrix Factorization- The out-of-sample RMSE of rating predictions is 1.1920696

Based on the testing NEURAL MATRIX FACTORIZATION MODEL works better.
